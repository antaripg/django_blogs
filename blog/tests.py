from django.test import TestCase
from blog.models import ArticleModel
from datetime import datetime
from django.utils import timezone
# Create your tests here.

# writing a test class - test suites

class ArticleTest(TestCase):
    # test cases are written as a methods

    def test_article_created_success(self):
        ArticleModel.objects.create(title="Test Article", category="Test category",
                                   author="Test Author", content="Test Content",
                                   created_at = datetime.now(tz=timezone.utc))
        article = ArticleModel.objects.get(title="Test Article")
        self.assertEqual(article.category, 'Test category')

class BlogPagesTest(TestCase):

    def test_home_page_content(self):
        res = self.client.get("/blog/")
        self.assertEqual(res.content, b"Welcome to my blog!")
