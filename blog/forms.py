from django.forms import ModelForm
from .models import ArticleModel

# Creating a form class meta


class ArticleForm(ModelForm):
    class Meta:
        model = ArticleModel
        fields = ["title", "category", "author", "content"]