from django.contrib import admin
from blog.models import ArticleModel
# Register your models here.

# Display the articles and article details

@admin.register(ArticleModel)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ("title", "author", "created_at")